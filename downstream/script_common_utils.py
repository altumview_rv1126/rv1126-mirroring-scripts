#!/usr/bin/env python

def slug_name(proj_name):
    retv = None
    # special handling: bitbucket slug-name cannot exceed 62 characters
    if proj_name == "rk-prebuilts-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf":
        proj_name = "rk-preb-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf"
    if len(proj_name) <= 62:
        retv = proj_name
    return retv

