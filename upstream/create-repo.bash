#!/bin/bash

# TODO: qy: may change this bitbucket_team to other proper names for the bitbucket workspace
# TODO: qy: change the bitbucket_user to the proper username and password combination
# modify the two lines to match the team name and user name and password:
bitbucket_team=altumview_rv1126
bitbucket_user="username:password"

# TODO: qy: can set the settings_local file to override the above variables
# if the file "settings_local" exists, source to get the two variables assigned:
if [ ! -z "${REPO_ENV_SCRIPT_DIR}" -a -f ${REPO_ENV_SCRIPT_DIR}/settings_local ]; then
  source ${REPO_ENV_SCRIPT_DIR}/settings_local
fi

# normalize the slug name for bitbucket:
prj_name_long=$( echo -n ${REPO_PROJECT} | tr '[:upper:]' '[:lower:]' )

    # transform slash in project names into dash as required by bitbucket:
    function name_trans() {
        PROJN="$1" # long name with slash in it
        lcnt=0
        NNAME=$(basename ${PROJN})
        DIR=$(dirname ${PROJN})
        while [ ! -z "${DIR}" -a ! "${DIR}" == "." ]; do
          SEG=$(basename ${DIR})
          DIR=$(dirname ${DIR})
          NNAME="${SEG}-${NNAME}"
          lcnt=$(( $lcnt + 1 ))
          if [ $lcnt -gt 10 ]; then
            break
          fi
        done

        # special handling: bitbucket slug-name cannot exceed 62 characters
        if [ "${NNAME}" == "rk-prebuilts-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf" ]; then
          NNAME="rk-preb-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf"
        fi

        echo "${NNAME}"
    }

prj_name=`name_trans ${prj_name_long}`

echo  
echo REPO_PROJECT ${REPO_PROJECT}  ${prj_name}
echo 

# create the repo on bitbucket: the project name can contain slash as REPO_PROJECT does.
echo '{ "name": "'${REPO_PROJECT}'", "is_private": true }' > input.json
# possible more options: -d '{"has_wiki": true, "is_private": true, "project": {"key": "PRJ_KEY"}}'

curl -X POST -v -u ${bitbucket_user} -H "Content-Type: application/json" \
  https://api.bitbucket.org/2.0/repositories/${bitbucket_team}/${prj_name} \
  -d @input.json
rc=$?

if [ $rc -ne 0 ]; then
	echo
        echo '#################################################'
	echo ' ' Failed!
        echo '#################################################'
	echo
fi

# log the names and results: 
echo REPO_PROJECT ${REPO_PROJECT} :: ${prj_name} :: ${REPO_I} :: $rc >> log-upstream-push-names

exit $rc


